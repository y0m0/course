# Final projects

You'll spend the next two weeks working in teams on an extended project.

## Learning objectives

### Goals for the week

Ask yourself the same two questions:

* Are you having fun?

* Are you a better developer than you were yesterday?

#### Primary goal

By the end of the week, the goal is to be able to answer "yes" to the week's primary question:

* **Can you use high-quality processes to build an extended project in a team?**

Production code is generally built by a team of developers. It is important that you leave Makers with the skills needed to work in a team, and this is the primary reason that project weeks exist in the curriculum.

The final projects also provide a safe environment for you to be creative and enjoy exploring new directions with your code.

* [Detailed learning objectives](https://github.com/makersacademy/course/blob/master/practice_project_week/learning_objectives.md)

## What is a successful project?

This is not a test, nor a challenge to see how many features you can cram in. Far more important is that you have:

* A fully tested project.
* Well-crafted code.
* A team that has worked well together using Agile methodologies.

## What technology should you use?

You can use any technology you like! It will be easier to use technologies you've already used at Makers.  But you can use ones that are completely new.

## Designing your project

It's highly recommended that you spend time designing your project.  To guide your design, you can use the [same method you used to design your practice project](../practice_project_week/project_design_workshop.md).

## Development workflow

Remember to use a [development workflow](https://github.com/makersacademy/course/blob/master/pills/development_workflow.md) similar to the one you used for Makersbnb and practice projects.

## MVP

Try to complete your MVP by Wednesday evening.

## XP values

If you're ever stuck, or facing a problem, turn to the [XP values](http://www.extremeprogramming.org/values.html) for guidance to how to act.

## Standups and retros

You'll have standups and retros in your teams.

## Graduation and project presentations

On Friday of week 12 (next week), you'll graduate.  At the graduation ceremony, each team will give a presentation about their project.  It'll be similar in structure and content to your practice project presentation.  But it'll be less technical, because many of the audience members (your friends, parents, relatives and S.O.s) will be non-technical.

See the [guidelines](../pills/final_project_presentations.md).

### Sequence & Schedule
* [Onsite](../sequence/onsite/final_projects.md)
* [Remote](../sequence/remote/final_projects.md)

