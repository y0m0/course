## Deploying to Heroku

[Back to the Challenge Map](00_challenge_map.md)

Now that you understand about the different database environments and have database cleaner configured, you might be wondering about a 'production' environment. Production is a live instance of your application that is used by the end-users. In general this will involve hosting your application somewhere. In this challenge we'll host our application on the Heroku cloud hosting service and configure the database for production.

## Learning Objectives covered

* Differentiate between `development` and `production` environments
* Understand that Heroku is a cloud hosting platform
* Be able to deploy a datamapper/postgres app to Heroku
* Be able to debug a datamapper/postgres app on Heroku

## To complete this challenge, you will need to

- [ ] Set up your app as a Heroku app
- [ ] Add a config file that tells Heroku how to start your app
- [ ] Configure the Heroku app to include a postgresql database
- [ ] Adjust the bookmark manager app so the database url will work with Heroku
- [ ] Deploy to Heroku and check that all the functionality works

## Resources

* [What is Heroku?](https://www.heroku.com/about)
* [Deploying a Ruby app to Heroku](https://devcenter.heroku.com/articles/getting-started-with-ruby-o)
* [Configuring DataMapper for Heroku](https://devcenter.heroku.com/articles/rack#using-datamapper-or-sequel)
* [Adding Postgres support to Heroku instance](https://devcenter.heroku.com/articles/heroku-postgresql)
* [Viewing your app's logs (getting visibility) on Heroku](https://devcenter.heroku.com/articles/logging)

## [Walkthrough](walkthroughs/14.md)
