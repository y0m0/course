# Bookmark Manager

This week, you will build a web app that stores web bookmarks in a database.

## Learning objectives

### Goals for the week

Ask yourself the same two questions:

- Are you having fun?

- Are you a better developer than you were yesterday?

#### Primary goal

By the end of the week, the goal is to be able to answer "yes" to the week's primary question:

- **Can you build a web app that uses a database?**

### Concepts and skills for the week

#### Concepts

- Object-relational mappers
- Relationships between data in a database
- SQL, the database query language
- Data encryption
- Rake
- HTML forms
- Web app deployment

#### Skills

- Designing data relationships using an object-relational mapper
- Designing a database schema
- Manipulating data using the CRUD cycle
- Structuring more complex MVC applications
- Refactoring more complex apps
- Creating user stories

As well as these things, you'll deepen your understanding of many of the [skills and concepts from week 3](../intro_to_the_web/00_challenge_map.md#concepts-and-skills-for-the-week).

## The project

You're going to build a bookmark manager.  A bookmark manager is a website to maintain a collection of URLs. You can use it to save a webpage you found useful. You can add tags to the webpages you saved to find them later. You can browse links other users have added.

### User Interface Sketch (Hi-Fi)

This is the basic view of the website. You will build it by working through the sequence of challenges. As you go on, we will be challenging you to extend the functionality of this website.

![](https://dchtm6r471mui.cloudfront.net/hackpad.com_jubMxdBrjni_p.52567_1380279073159_Screen%20Shot%202013-09-27%20at%2011.06.12.png "Bookmark Manager")

## Learning, not challenge progress

There is a **lot** of challenge material this week.  You probably won't get through it all.  As always at Makers, focus on deep learning, rather than progression through the challenges.

### Sequence & Schedule
* [Onsite](../sequence/onsite/week04.md)
* [Remote](../sequence/remote/week04.md)

## Challenges

 1. [Creating User stories](01_creating_user_stories.md)
 2. [Understanding Databases](02_understanding_databases.md)
 3. [Setting up a Database](03_setting_up_a_database.md)
 4. [Creating Your First Table](04_creating_your_first_table.md)
 5. [Manipulating Table Data](05_manipulating_table_data.md)
 6. [Using DataMapper](06_using_data_mapper.md)
 7. [Research CRUD](07_research_crud.md)
 8. [Viewing Links](08_viewing_links.md)
 9. [Creating a Link Model](09_creating_a_link_model.md)
 10. [Creating a Modular Sinatra App](10_creating_a_modular_sinatra_app.md)
 11. [Creating Links](11_creating_links.md)
 12. [Configuring DatabaseCleaner](12_configuring_database_cleaner.md)
 13. [Configuring the Rack Env](13_configuring_the_rack_env.md)
 14. [Deploying to Heroku](14_deploying_to_heroku.md)
 15. [Tagging Links](15_tagging_links.md)
 16. [Filtering by tag](16_filtering_by_tag.md)
 17. [Multiple Tags](17_multiple_tags.md)
 18. [Adding User Accounts](18_adding_user_accounts.md)
 19. [Password confirmation](19_password_confirmation.md)
 20. [Improving Sign Up Workflow](20_improving_sign_up_workflow.md)
 21. [Levels of Validation](21_levels_of_validation.md)
 22. [Preventing Duplicate Registrations](22_preventing_duplicate_registrations.md)
 23. [Signing in](23_signing_in.md)
 24. [Signing Out](24_signing_out.md)
 25. [Refactoring](25_refactoring.md)
 26. [Password Recovery](26_password_recovery.md)
 27. [Mailing the Token](27_mailing_the_token.md)
