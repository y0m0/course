# Week One Structure

#### Monday

Monday is [Day one](./day_one.md)

#### Tuesday, Wednesday, Thursday

[Challenges mid-week schedule](./challenges_mid_week_schedule.md)

#### Friday

[Challenges Friday schedule](./challenges_friday_schedule.md)
