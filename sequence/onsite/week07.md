# Week Seven Structure

#### Monday

[Challenges Monday schedule](./challenges_monday_schedule.md)

#### Tuesday

- 09.30 - Byte 1 [standup](../../pills/student_standups.md)
- 09.45 - Byte 2 [standup](../../pills/student_standups.md)
- 10.00 - [Possible skills workshop](../../pills/learning_at_makers.md#skills-workshops) or group code review with coach
- 18.00 - [Daily feedback](../../pills/learning_at_makers.md#daily-feedback)

#### Wednesday

- 09.30 - Byte 1 [standup](../../pills/student_standups.md)
- 09.45 - Byte 2 [standup](../../pills/student_standups.md)
- 10.00 - [Possible skills workshop](../../pills/learning_at_makers.md#skills-workshops) or group code review with coach
- 14.30 - [Process surgery](https://github.com/makersacademy/course/blob/master/pills/process_surgery.md)
- 18.00 - [Daily feedback](../../pills/learning_at_makers.md#daily-feedback)

#### Thursday

- 09.30 - Byte 1 [standup](../../pills/student_standups.md)
- 09.45 - Byte 2 [standup](../../pills/student_standups.md)
- 10.00 - [Possible skills workshop](../../pills/learning_at_makers.md#skills-workshops) or group code review with coach
- 18.00 - [Daily feedback](../../pills/learning_at_makers.md#daily-feedback)

#### Friday

- 09.30 - Byte 1 [standup](../../pills/student_standups.md)
- 09.45 - Byte 2 [standup](../../pills/student_standups.md)
- 10.00 - [Possible skills workshop](../../pills/learning_at_makers.md#skills-workshops) or group code review with coach
- 17.00 - Confidence Workshop.
- 17.30 - [Retro](https://github.com/makersacademy/course/blob/master/pills/student_retrospective.md). Coach-facilitated.
- 18.00 - Announcements for the weekend.
- 18.00 - [Daily feedback](../../pills/learning_at_makers.md#daily-feedback)
