# Challenges mid-week schdeule

- 09.30 - Byte 1 [standup](../../pills/student_standups.md)
- 09.45 - Byte 2 [standup](../../pills/student_standups.md)
- 10.00 - [Possible skills workshop](../../pills/learning_at_makers.md#skills-workshops)
- 18.00 - [Daily feedback](../../pills/learning_at_makers.md#daily-feedback)
