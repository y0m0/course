# Week Three Structure

#### Monday

[Challenges Monday schedule](./challenges_monday_schedule.md)

#### Tuesday, Wednesday, Thursday

[Challenges mid-week schedule](./challenges_mid_week_schedule.md)

#### Friday

[Challenges Friday schedule](./challenges_friday_schedule.md)
