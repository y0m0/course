# Your first day at Makers Academy

If you have any questions, just ask!

## Schedule for the first day

* 08.30 - 09.30 Breakfast
* 09.30 - 10.30 Welcome to Makers (Coach)
* 10.30 - 11.00 Name tag making (Coach, staff, all students)
* 11.00 - 11.30 Get and setup your laptops
* 11.30 - 12.30 Life at Makers/Intro to Emotional Intelligence Curriculum (Dana)
* 12.30 - 14.00 Lunch and more laptop setup
* 14.00 - 14.30 Meditation
* 14.30 - 15.15 [Learning at Makers](https://github.com/makersacademy/course/blob/master/pills/learning_at_makers.md) (Sam)
* 15.15 - 17.30 [Pairing challenges](https://github.com/makersacademy/skills-workshops/tree/master/week-1/pairing_challenges)
* 17.30 - 18.30 Social with pizza

## Rough weekly schedule

See [:pill: here](https://github.com/makersacademy/course/blob/master/pills/example_schedule.md) for the rough weekly schedule.

## Learning at Makers

[A summary](https://github.com/makersacademy/course/blob/master/pills/learning_at_makers.md).

## Health and safety

[A summary](https://github.com/makersacademy/course/blob/master/pills/health_and_safety.md).

## Facilities

[Facilities](https://github.com/makersacademy/course/blob/master/pills/facilities.md).

## Low noise policy

[A summary](https://github.com/makersacademy/course/blob/master/pills/low_noise_policy.md).

## Seating plan

[A summary](https://github.com/makersacademy/course/blob/master/pills/seating_plan.md).

## Laptops

### Getting your laptop (onsite cohorts)

At 11.00am on the first day, go to the mezzanine and ask for Cata.  To get to the mezzanine, go to floor 3, go into the room with all the desks and monitors, then go up the wooden stairs.

### Setting up your laptop

Have a look at [Prepare to Code](http://www.preparetocode.io/).  It has a guide for what software to install to prepare for the Makers course.

### Installing Makersinit

This is a Ruby gem that will automatically add your Git commits to your student profile.  This will help us see that you're doing OK.  Please install it.  Thanks!

https://github.com/makersacademy/makersinit
