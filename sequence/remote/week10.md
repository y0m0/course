# Week Ten Structure

#### Monday

* Everyone who is looking for a job as a developer will have the opportunity to have a 121 with one of the careers team during week 10. We'll share the sign up sheet with you at the end of week 9. Slots will be available at various times throughout the week.

* 9.30am: Your coach will introduce today's tech test: [Bank](../../individual_challenges/bank_tech_test.md)
* 10am: You'll work solo on the tech test.
* 5.00pm: You'll review your code in pairs.
* 5.30pm: We'll review your code in a group.

#### Tuesday
* 9.45: Your coach will introduce the next tech test: [Database Server](../../individual_challenges/database_server.md)
* 9.30am: We'll do a mini retro on yesterday's work.
* 10am - 11pm: Careers session: Practice Tech Interviews
* 3.00 - 4pm: Careers session: How to network
* 5.15pm: You'll work solo on the tech test.

#### Wednesday
* 9.30am: We'll do a mini retro on yesterday's work.
* 9.45: Your coach will introduce the next tech test: [Gilded rose](../../individual_challenges/gilded_rose.md)
* 5.00pm: You'll review your code in pairs.
* 5.30pm: We'll review your code in a group.

#### Thursday

* 9.30am: We'll do a mini retro on yesterday's work.
* 9.45am: Your coach will introduce the next tech test: [tic-tac-toe](../../individual_challenges/tic_tac_toe.md)
* 10am: You'll work solo on the tech test.

#### Friday

* 9.30am: We'll do a mini retro on yesterday's work.
* 10am: [Final projects idea generation workshop](https://github.com/makersacademy/skills-workshops/blob/master/project_idea_generation_workshop.md)
* 11:30am: You'll continue to work solo on the tic-tac-toe tech test.
* 3.00pm: You'll review your code in pairs.
* 3.30pm: Debugging Session
* 4.30pm: Retro
