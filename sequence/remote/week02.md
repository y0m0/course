# Week Two Structure

#### Tuesday

- 9.30: Peer-led code review
- 10.00: Group code review and Q&A with coach
- 11.00: Weekly kickoff workshop

#### Wednesday, Thursday
- 9.30: Workshop
- 10.30: Standup 0
- 10.45: Standup 1
- 11.00: Standup 2

#### Friday
- 9.30: Workshop
- 10.30: Concepts, skills, behaviours workshop
- 11.00: Confidence workshop
- 11.30: [Retro](https://github.com/makersacademy/course/blob/master/pills/student_retrospective.md)
- 12.00: Announcements for the weekend
