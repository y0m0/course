# Week Seven Structure

#### Monday

- 9.30: Peer-led code review
- 10.30: Group code review and Q&A with coach
- 11.00: Weekly kickoff workshop

#### Tuesday
- 9.30: Standups (Student organised)
- 10.00: Workshop

#### Wednesday, Thursday
- 9.30: Standups (Student organised)
- 17.00: Process surgery

#### Thursday
- 21:00 musical forms workshop

#### Friday
- 4.30: Skills, Behaviors, Concepts
- 5.00: Confidence Workshop
- 5.30: [Retro](https://github.com/makersacademy/course/blob/master/pills/student_retrospective.md)
- 6.00: Announcements for the weekend
