# Week Nine Structure - Practice Project Week!

#### Monday

- 9.30: Project idea generation workshop
- 10.00: Pair code review
- 11.00: Group code review & Q&A with coach
- 12.00: Kick off workshop and team announcement

#### Tuesday, Wednesday, Thursday

- Teams organise their own schedules

#### Friday

- 4.00: Practice project presentations
- 5.00: Confidence Workshop
- 5.30: [Retro](https://github.com/makersacademy/course/blob/master/pills/student_retrospective.md)
- 6.00: Announcements for the weekend
