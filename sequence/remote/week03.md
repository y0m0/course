# Week Three Structure

#### Monday

- 9.30: Peer-led code review
- 10.00: Group code review and Q&A with coach
- 11.00: Weekly kickoff workshop

#### Tuesday
- 9.30: Workshop
- 10.30: Standups
- 15.00: Check-in session with Dana

#### Wednesday, Thursday
- 9.30: Workshop
- 10.30: Standups

#### Friday
- 4.30: Skills, concepts, behaviours reflection
- 5.00: Confidence Workshop
- 5.30: [Retro](https://github.com/makersacademy/course/blob/master/pills/student_retrospective.md)
- 6.00: Announcements for the weekend
