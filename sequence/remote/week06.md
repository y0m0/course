# Week Six Structure

#### Monday

- 9.30: Weekly kickoff workshop & Project Team Reveal

#### Tuesday
- 10.00: Optional workshop (Ruby fundamentals)

#### Wednesday
- Project teams set your own schedule

#### Thursday
- 9.30: Optional workshop (TBC)
- 14.30 Careers session

#### Friday
- 9.30: Retro planning, confidence workshop, weekend announcements
- 16.00: Individual group retros (organised in your groups)
