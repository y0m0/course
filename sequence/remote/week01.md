# Week One Structure

#### Monday

Monday is [Day one](./day_one.md)

#### Tuesday
- 9.30: Workshop
- 10.30: Standup 0
- 10.45: Standup 1
- 11.00: Standup 2

#### Wednesday
- 9.30: Workshop
- 10.30: Standup 0
- 10.50: Standup 1
- 11.10: Standup 2
- 11.30: Becoming a Knowledge worker (careers team)
- 12.00: Meet the careers team

#### Fursday
- 9.30: Workshop
- 14.00: Gender diversity at Makers
- 16.00: Concepts, Skills and Behaviours reflection.
- 17.00: Confidence Workshop.
- 17.30: [Retro](https://github.com/makersacademy/course/blob/master/pills/student_retrospective.md). Coach-facilitated.
- 18.00: Announcements for the weekend.

#### Weekend challenge

- [Airport challenge](https://github.com/makersacademy/airport_challenge).
- Continue with Boris Bikes tomorrow (pairs will be generated).
- Monday - create mind map / semantic map fo concepts listed [here](https://docs.google.com/spreadsheets/d/1h0yf-99zZ5V3_XfJVw55d_SE21z1g9sdS-1eL5N0iEk/edit#gid=1472472789).
- (optional) Make a video of fully TDD'd FizzBuzz. Can you do it in less than 5 minutes? (Record for a student is around 4mins, 30 secs, with well named methods, not taking any shortcuts)
- (optional) Write a blog post about TDD - maybe an intro for someone new to it, or a particularly challenging problem you faced.
