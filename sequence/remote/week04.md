# Week Four Structure

#### Tuesday

- 9.30: Peer-led code review
- 10.30: Group code review and Q&A with coach
- 11.00: Weekly kickoff workshop

#### Wednesday

- 9.30: Standups (Student organised)

#### Thursday

- 9.30: Workshop
- 10.00: Standups (Student organised)
- 13.30: LunchnLearn

#### Friday

- 9.30: Workshop
- 4.30: Skills, Concepts, Behaviours
- 5.00: Confidence Workshop
- 5.30: [Retro](https://github.com/makersacademy/course/blob/master/pills/student_retrospective.md)
- 6.00: Announcements for the weekend
