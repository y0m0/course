# Rough weekly schedule

### Office hours

* Monday to Friday. Core hours: 09.30 to 18.00. You can stay until the office is closed at 7pm.

* Saturday and Sunday.  The office is closed.

### Programming

#### Monday

* Morning - Code review of [weekend challenge](./learning_at_makers.md#weekend-challenges)
* Late morning - Kick off for the week
* Late morning - Release [weekly challenges](./learning_at_makers.md#challenge-based-learning)
* 18.00 - [Daily feedback](./learning_at_makers.md#daily-feedback)

#### Tuesday, Wednesday, Thursday

* 09.30 - Byte 1 [standup](./student_standups.md)
* 09.45 - Byte 2 [standup](./student_standups.md)
* 10.00 - [Possible skills workshop](./learning_at_makers.md#skills-workshops)
* 18.00 - [Daily feedback](./learning_at_makers.md#daily-feedback)

#### Friday

* 09.30 - Byte 1 [standup](./student_standups.md)
* 09.45 - Byte 2 [standup](./student_standups.md)
* 10.00 - [Possible skills workshop](./learning_at_makers.md#skills-workshops)
* 17.00 - [Confidence survey](./confidence_survey.md)
* 17.30 - [Weekly retro](./student_retrospective.md)
* 18.00 - [Weekend challenge](./learning_at_makers.md#weekend-challenges) release
* 18.00 - [Daily feedback](./learning_at_makers.md#daily-feedback)

### Joy

#### Every weekday

* 14.00 to 14.30 - Meditation

#### Tuesday and Thursday

* 17.00 to 18.00 - Yoga

### Careers

* From week 6, there will be one careers session per week.
