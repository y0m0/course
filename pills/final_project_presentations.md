# Final Project Presentations

Here are some guidelines for your final project presentations:

* The time limit is usually 5 minutes (this depends on the number of projects, your coach will advise if the time limit is different).  Practice, practice, practice to ensure you come in under the time limit.  You'll be cut off at 5 minutes!

* To guide you on what to include, think what you found interesting about other people’s presentations.

* If appropriate, make it funny. If not, make it engaging.

* Explain very briefly a scenario where someone might use your app.

* Make a video that demonstrates a user using your app.  One of your team can describe what's going on as the video plays.  No need for a live demo (every time I've done a live demo, at least one thing has gone wrong).

* Describe any interesting struggles you had with teamwork, learning or programming.  Describe how you overcame them.  Focus on behaviours and skills.  Be specific.  Don't say, "we communicated well".  Say, "We had a miscommunication where two pairs worked on the same feature.  We decided in the future to..."

* Describe the architecture of your project.  Make sure a non-technical audience will be able to understand your description.  Don't put up the wall of death (the slide that is just library and framework logos).  Instead, perhaps describe how information flows through your architecture.

* There's no need to talk about the future direction of the project.  Focus your presentation on what you've already achieved.

### Important instructions: [Remote](../sequence/remote/demo_day_presentations.md) | [Onsite](../sequence/onsite/demo_day_presentations.md)
