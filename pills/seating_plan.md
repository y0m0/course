# Seating plan

```
    (door)
             PP
JJJJ         PP
JJJJ  JJJJ
      JJJJ   PP
             PP
JJ  OO  OO
JJ  OO  OO   PP
DJ  OO  OO   PP
CJ  OO  OO
CJ  OO  OO   PP
CJ  OO  OO   PP

J: Junior
C: Coach
D: Dana
O: Mid or senior or, if space, an alumni
P: Mid or senior project team member
```
